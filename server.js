/**
 * Created by arynas on 19/06/17.
 */

const express = require('express');
const bodyParser=  require('body-parser');
const app = express();
const MongoClient = require('mongodb').MongoClient;

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static('public'));

app.use(bodyParser.json());

module.exports = app;

var db;

MongoClient.connect('mongodb://root:root@ds131512.mlab.com:31512/crud_nodejs_mongodb', function (err, database) {
    if (err) return console.log(err);
    db = database;
    app.listen(3000, function () {
        console.log('Listening on 3000');
    });
});

app.get('/', function (req, res) {

    db.collection('names').find().toArray(function (err, result) {
        if (err) return console.log(err);
        // renders index.ejs
        res.render('index.ejs',{names: result});
    });

    // res.sendFile(__dirname + '/index.html')
});

app.post('/send', function (req, res) {
    db.collection('names').save(req.body, function (err, result) {
        if (err) return console.log(err);

        console.log('Saved to database!');
        res.redirect('/');
    });
});

app.put('/names', function (req, res) {
    db.collection('names')
        .findOneAndUpdate({first_name: 'Sergio'}, {
            $set:{
                first_name: req.body.first_name,
                last_name: req.body.last_name
            }
        },{
            sort: {_id: -1},
            upsert: true
        }, function (err, result) {
            if (err) return res.send(err);
            res.send(result);
        })
});

app.delete('/names', function(req, res) {
    db.collection('names').findOneAndDelete({first_name: req.body.first_name},
        function (err, result) {
        if (err) return res.send(500, err);
        res.send({message: 'Deleted!'})
    })
});

