Simple CRUD with NodeJS & MongoDB!
===================

Requirements:

1. Node
2. Express
3. MongoDb

Thanks on Zell Liew for <a href="https://zellwk.com/blog/crud-express-mongodb/">his tutorial</a>.