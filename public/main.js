/**
 * Created by arynas on 20/06/17.
 */

var update = document.getElementById('update');

update.addEventListener('click', function () {
    fetch('names', {
        method: 'put',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            'first_name': 'Cristiano',
            'last_name': 'Ronaldo'
            })
            }).then( function(data) {
                console.log(data);
                window.location.reload()
        })
});

var del = document.getElementById('delete');

del.addEventListener('click', function () {
    fetch('names', {
        method: 'delete',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "first_name": "Cristiano"
        })
        }).then( function (res) {
            if (res.ok) return res.json()
        }).then( function(data) {
            console.log(data);
        window.location.reload()
    });
});